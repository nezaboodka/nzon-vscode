# Installation

## Windows

Add extension:

``` shelll
cd %USERPROFILE%\.vscode\extensions
git clone https://gitlab.com/ychetyrko/nzon-vscode.git
```

Reload VS Code

## Mac/Linux

```
cd ~/.vscode/extensions
git clone https://gitlab.com/ychetyrko/nzon-vscode.git
```
